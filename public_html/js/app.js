/* 
 * The MIT License
 *
 * Copyright 2015 Maximillian M..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* global rawData */

var time = [];
var objects = [];
  
var currentTime = 0;
rawData.forEach(function(raw) {
  if (raw.timestamp !== currentTime) {
    var date = new Date(raw.timestamp);
    time.push(date.getUTCHours() + 3 + ':' + date.getUTCMinutes());
    currentTime = raw.timestamp;
  }
  if (!objects[raw.id]) {
    objects[raw.id] = {
      university: raw.university,
      team: raw.command,
      place: [ raw.place ],
      passed: [ raw.eq ],
      penalty: [ raw.penalty ]
    };
  } else {
    objects[raw.id].place.push(raw.place);
    objects[raw.id].passed.push(raw.eq);
    objects[raw.id].penalty.push(raw.penalty);
  }
});
  
var dataset = [];
var i = 0;
objects.forEach(function(obj) {
  var data = {};
  data.name = obj.university;
  data.data = obj.place;
    
  dataset[i] = data;
  i++;
});
  
console.log(time);
//  dataset = dataset.slice(0, 10);
  
$(function() {
  $('#chart').highcharts({
    chart: {
      type: 'line',
      zoomType: 'x'
    },
    plotOptions: {
      series: {
        marker: {
          enabled: false
        },
        point: {
          events: {
            click: function() {
              this.series.hide();
            }
          }
        }
      }
    },
    title: {
      text: 'ACM ICPC NEERC Southern Subregion'
    },
    yAxis: {
      title: {
        text: 'Position'
      },
      reversed: true
    },
    xAxis: {
      text: 'Time',
      categories: time
    },
    series: dataset
  });
});